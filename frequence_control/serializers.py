from .models import *
from rest_framework import serializers

class StatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttendanceStates
        fields = ('id', 'description')