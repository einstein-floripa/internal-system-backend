from django.urls import path
from django.utils import timezone
from django.conf.urls import url
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import viewsets

from internal.models import Tests, TestsSubscriptions, StudentClassRoom


class TestsList(APIView):
    def get(self, request, num=5):
        tests = list(Tests.objects.all())
        # Take the last num
        if len(tests) > num:
            tests = tests[-num:]

        return Response([{'id': t.id, 'label': str(t)} for t in tests], status=status.HTTP_200_OK)


class SubscriptionsList(APIView):
    def get(self, request, test_id):
        exists = Tests.objects.filter(id=test_id).exists()

        if not exists:
            return Response("Test doesn't exist!", status=status.HTTP_400_BAD_REQUEST)

        subscriptions = TestsSubscriptions.objects.filter(test_id=test_id)

        subscribers = []

        for sub in subscriptions:
            student_class_query = StudentClassRoom.objects.filter(
                student=sub.user)
            if student_class_query.exists():
                student_class = student_class_query[0].class_room.name
            else:
                student_class = 'Unknown'

            name = sub.user.full_name
            cpf = sub.user.cpf
            language = str(sub.language)
            quota = str(sub.quota)
            course = str(sub.course)

            subscribers.append({
                'name': name,
                'cpf': cpf,
                'language': language,
                'quota': quota,
                'course': course,
                'class': student_class
            })

        return Response(subscribers, status=status.HTTP_200_OK)
