from django.contrib import admin
from django.urls import include, path
from rest_framework import routers, permissions, authentication
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

#teste


schema_view = get_schema_view(
    openapi.Info(
        title="Internal System docs",
        default_version='v1',
        description="Baisc documentation for endpoints",
        contact=openapi.Contact(email="matrix@einsteinfloripa.com.br"),
        license=openapi.License(name="MIT License"),
    ),
    public=False,
    permission_classes=(permissions.IsAuthenticated,),
    authentication_classes=(authentication.BasicAuthentication,),
)

urlpatterns = [
    path('swagger/', schema_view.with_ui('swagger',
                                         cache_timeout=0), name='schema-swagger-ui'),
    path('internal/', include('internal.urls')),
    path('admin/', admin.site.urls),
    path('attendance/', include('frequence_control.urls')),
    path('corrector/', include('corrector.urls')),
]
