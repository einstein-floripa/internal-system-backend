from django.urls import path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from . import views


urlpatterns = [
    path('certification/', views.StudentCertificationView.as_view(),
         name='Certificaçãos'),
    path('authentication/', views.StudentCertificationValidationView.as_view(),
         name='Autenticação'),
    path('tests/info-and-subs/', views.TestsInformation.as_view(),
         name='Informações e inscrições em simulados'),
    path('tests/create-and-modify/', views.TestCreationView.as_view(),
         name='Criação e modificação de simulados'),
    path('recovery/trigger', views.PasswordRecovery.as_view(),
         name='Gerar token para resetar senhas'),
    path('recovery/reset', views.PasswordReset.as_view(),
         name='Resetar senhas'),
    path('resources', views.UserResource.as_view(),
         name='Recursos para usuários'),
    path('api-token-auth/', views.ObtainExpiringAuthToken.as_view()),
    path('user-info/', views.UserInformation.as_view()),
    path('contact/<label>', views.ContactsInformation.as_view()),
    path('schedule/', views.ClassSchedules.as_view()),
    path('schedule/<class_label>', views.ClassSchedules.as_view()),
    path('schedule/subjects/', views.SubjectsView.as_view(),
         name='List de matérias'),
    path('occurrences/', views.OccurenceOperations.as_view()),
    path('occurrences/solveds/', views.SolvedOccurrenceView.as_view()),
    path('occurrences/solveds/<num_occur>',
         views.SolvedOccurrenceView.as_view()),
    path('occurrences/unsolveds/', views.UnsolvedOccurrenceView.as_view()),
    path('occurrences/types/', views.OccurrenceTypeViewSet.as_view({'get': 'list'})),
    path('occurrences/create/', views.OccurrenceCreationView.as_view()),
    path('process/<dpt>', views.ProcessView.as_view()),
    path('process/create/', views.ProcessCreationView.as_view()),
    path('process/delete/', views.ProcessCreationView.as_view()),
]
