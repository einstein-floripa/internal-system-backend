from django.urls import path
from django.conf.urls import url

from . import views


urlpatterns = [
    path('testlist/', views.TestsList.as_view()),
    path('testlist/<int:num>', views.TestsList.as_view()),
    path('subscribers/<int:test_id>', views.SubscriptionsList.as_view()),
]
