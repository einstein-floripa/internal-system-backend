from .models import *
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            name=validated_data['email'],
            register=validated_data['register'],
            cpf=validated_data['cpf'],
            type=validated_data['type'],
            address=validated_data['address'],
            email=validated_data['email'],
            phone=validated_data['phone'],
            birthday=validated_data['birthday'],
            ocupation=validated_data['ocupation'],
            gender=validated_data['gender'],
            main_contact=validated_data['main_contact'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tests
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class QuotaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quota
        fields = '__all__'


class SubscriptionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestsSubscriptions
        fields = '__all__'


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'


class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields = '__all__'


class OcupationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ocupation
        fields = '__all__'


class GenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gender
        fields = '__all__'


class OccurrenceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OccurrenceType
        fields = '__all__'

class ProcesseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = '__all__'

class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = '__all__'


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ['link', 'description']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassSubject
        fields = ['__all__']
