from django.core.management.base import BaseCommand
from loguru import logger

import csv
import os

from internal.models import User, Resource


class Command(BaseCommand):
    help = 'Add resources for users from a csv file.'
    data = {
        'user_cpf': '<owner_cpf - string>',
        'description': '<LAST_NAME - string>',
        'link': '<RESOURCE_LINK - string>',
        'str_info': '<RESOURCE_TEXT - string - displayed on adm>',

    }

    def add_arguments(self, parser):
        # Argument to receive
        parser.add_argument('--csv_file',
                            type=str,
                            help='File with resources. --template for genrating template with column names')

        # Argumemnt to generate template of csv file
        parser.add_argument('--template',
                            action='store_true',
                            help='Generate template of csv file')
        parser.add_argument('--output',
                            type=str,
                            help='Output filename')

    def generate_template_file(self, filename):
        with open(filename, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=self.data.keys())
            writer.writeheader()
            writer.writerow(self.data)

    def save_users(self, filename):
        with open(filename) as f:
            reader = csv.DictReader(f)

            for row in reader:
                cpf = row['user_cpf'].replace('.', '').replace('-', '')
                user = User.objects.get(cpf=cpf)
                r = Resource(
                    owner=user,
                    description=row['description'],
                    str_info=row['str_info'],
                    link=row['link'],
                )

                r.save()

    def validate_archive(self, filename):
        with open(filename) as f:
            reader = csv.DictReader(f)
            line = 0
            for row in reader:
                cpf = row['user_cpf'].replace('.', '').replace('-', '')
                not_exists = User.objects.filter(cpf=cpf).count() != 1
                if not_exists:
                    return (
                        False,
                        f'{row["user_cpf"]} on line {line} do not exists on users!'
                    )

                max_len = Resource._meta.get_field('str_info').max_length
                if len(row['str_info']) > max_len:
                    return (
                        False,
                        f'str_info on line {line} is too large! ' +
                        f'Max len is {max_len}'
                    )

                line += 1

        return (True, 'Validation successful')

    def handle(self, *args, **options):
        if options['template']:
            # Grant that if a template is already in the folder,
            # the script creates a new one, don't messing with a
            # possible template with data in
            filename = 'add_resource_template.csv'
            while True:
                if os.path.isfile(filename):
                    filename = 'new_' + filename
                else:
                    break

            # Set filename to argument, if it have
            if options['output']:
                filename = options['output']

            logger.info(f'Generating template file as `{filename}`')
            self.generate_template_file(filename)
            logger.info('File generated!')

        elif options['csv_file']:
            if not os.path.isfile(options['csv_file']):
                logger.error(f'File `{options["csv_file"]}` does not exist!`')
                return

            valid, message = self.validate_archive(options['csv_file'])
            if valid:
                logger.info(message)
                logger.info('Saving resources')
                self.save_users(options['csv_file'])
                logger.info('Resources saved!')
            else:
                logger.error(message)

        else:
            self.print_help(__file__, '')
