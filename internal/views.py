import time
import datetime

from django.shortcuts import render
from django.http import HttpResponse
from django.utils.timezone import now
from rest_framework import status
from django.db.models import ObjectDoesNotExist
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .serializers import *
from rest_framework import viewsets
from drf_yasg.utils import swagger_auto_schema

from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
from django.utils import timezone

from internal_system_backend.utils import render_to_pdf, months


class UserTypeViewSet(viewsets.ModelViewSet):
    queryset = UserType.objects.all()
    serializer_class = UserTypeSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class OcupationViewSet(viewsets.ModelViewSet):
    queryset = Ocupation.objects.all()
    serializer_class = OcupationSerializer


class GenderViewSet(viewsets.ModelViewSet):
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer


class OccurrenceTypeViewSet(viewsets.ModelViewSet):
    queryset = OccurrenceType.objects.all()
    serializer_class = OccurrenceTypeSerializer


class UniversityViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class QuotaViewSet(viewsets.ModelViewSet):
    queryset = Quota.objects.all()
    serializer_class = QuotaSerializer


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class TestsViewSet(viewsets.ModelViewSet):
    queryset = Tests.objects.all()
    serializer_class = TestSerializer


class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class TestsInformation(APIView):
    @swagger_auto_schema(
        operation_description='Get list of future tests',
        responses={200: 'Return json with tests and information about user ' +
                        'subscription, if it exists'
                   },
    )
    def get(self, request, *args, **kwargs):
        user = User.objects.get(email=request._user)
        # Retorna lista de simulados
        tests = Tests.objects.filter(test_datetime__gt=now())
        courses = Course.objects.all()
        quotas = Quota.objects.all()
        languages = Language.objects.all()

        serial_tests = TestSerializer(tests, many=True).data
        serial_courses = CourseSerializer(courses, many=True).data
        serial_quotas = QuotaSerializer(quotas, many=True).data
        serial_languages = LanguageSerializer(languages, many=True).data

        for test in serial_tests:
            test['user_sub'] = TestsSubscriptions.objects.filter(
                user=user.id, test=test['id']).exists()

            if test['user_sub']:
                subscription = TestsSubscriptions.objects.get(
                    user=user.id, test=test['id'])
                test['user_sub_data'] = SubscriptionsSerializer(
                    subscription).data

        return Response({
            'tests': serial_tests,
            'quotas': serial_quotas,
            'courses': serial_courses,
            'langs': serial_languages,
        })

    @swagger_auto_schema(
        operation_description='Subscribe user',
        responses={200: 'Return json with tests and information about user ' +
                        'subscription, if it exists'
                   },
    )
    def post(self, request, *args, **kwargs):
        user = User.objects.get(email=request._user)

        try:
            id_test = request.data['id_test']
            id_course = request.data['id_course']
            id_quota = request.data['id_quota']
            id_lang = request.data['id_lang']

            test = Tests.objects.get(id=id_test)
            course = Course.objects.get(id=id_course)
            quota = Quota.objects.get(id=id_quota)
            lang = Language.objects.get(id=id_lang)

            # Impede inscrição caso as inscrições estejam fechadas
            if test.close_sub < now():
                return Response({'done': False,
                                 'detail': 'Tests subscriptions are not open'},
                                status=status.HTTP_400_BAD_REQUEST)

            # Pega a ocorrência que já existe e atualiza ou cria uma nova instancia
            # para respeitar o unique_together definido. Assim, um usuário não pode
            # ter mais de uma inscrição no banco de dados para um determinado
            # simulado
            sub, created = TestsSubscriptions.objects.update_or_create(test=test,
                                                                       user=user,
                                                                       defaults={
                                                                           'quota': quota,
                                                                           'course': course,
                                                                           'language': lang
                                                                       })
            return Response({'done': True,
                             'detail': 'Success'},
                            status=status.HTTP_200_OK)
        except:
            return Response({'done': False,
                             'detail': 'Fail. Check your params.'},
                            status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        user = User.objects.get(email=request._user)
        try:
            id_test = request.data['id_test']
            test = Tests.objects.get(id=id_test)
            sub = TestsSubscriptions.objects.get(test=test, user=user)
            sub.delete()

            return Response({'done': True,
                             'detail': 'Subscription of {} deleted'.format(user.first_name)},
                            status=200)

        except:
            return Response({'done': False,
                             'detail': 'Fail. Check your params.'},
                            status=status.HTTP_400_BAD_REQUEST)


class StudentCertificationView(APIView):
    def get(self, request, *args, **kwargs):
        user = User.objects.get(email=request._user)

        if Ocupation.objects.get(id=user.ocupation_id).label != 'STU':
            return Response({'done': False,
                             'detail': 'Unauthorized - Must be a student'},
                            status=status.HTTP_403_FORBIDDEN)

        now = datetime.datetime.now()
        cpf = user.cpf
        formated_cpf = '{}.{}.{}-{}'.format(cpf[0:3],
                                            cpf[3:6], cpf[6:9], cpf[9:11])
        data = {
            'YEAR': user.register_day.year,
            'DATE': '{} de {} de {}'.format(now.day, months[now.month], now.year),
            'STUDENT_NAME': user.full_name,
            'STUDENT_CPF': formated_cpf,
            'OFFICE_ADDRESS': settings.OFFICE_ADDRESS,
            'AUTHENTICAION_URL': settings.AUTHENTICAION_URL,
        }

        pdf = render_to_pdf('pdf/registry_certification.html', data)
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "atestado.pdf"
        response['Content-Disposition'] = 'attachment; filename={}'.format(
            filename)
        return response


class StudentCertificationValidationView(APIView):
    authentication_classes = ()
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        cpf = request.query_params.get('cpf')
        if cpf is None:
            return Response({
                'done': False,
                'detail': 'cpf must be informed',
            }, status=400)

        # Granting cpf formatting
        cpf = cpf.replace('.', '').replace('-', '').zfill(11)
        user = None
        try:
            user = User.objects.get(cpf=cpf)
        except ObjectDoesNotExist:
            pass

        response_not_find = Response({
            'done': False,
            'detail': 'cpf not find',
        }, status=200)

        # If user don't exists or user is not a student, we return as it was not find.
        # Avoiding expose information os others cpfs
        if user is None:
            return response_not_find

        user_type = UserType.objects.get(id=user.type_id)

        if user_type.label != 'Aluno(a)':
            return response_not_find

        # If exists and is 'Aluno'
        return Response({
            'done': True,
            'detail': 'cpf found',
            'cpf_name': user.first_name + user.last_name,
        }, status=200)


class ObtainExpiringAuthToken(ObtainAuthToken):
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(
                user=serializer.validated_data['user'])

            if not created:
                # update the created time of the token to keep it valid
                token.created = now()
                token.save()

            return Response({'token': token.key,
                             'user_data': {
                                 'first_name': serializer.validated_data['user'].first_name,
                                 'type': serializer.validated_data['user'].type.id,
                                 'ocupation': serializer.validated_data['user'].ocupation.id
                             }})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordRecovery(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        email = request.data['email']
        exists = User.objects.filter(email=email).count() == 1
        if exists:
            user = User.objects.get(email=email)

            # Generate recovery token on DB
            rec, create = PasswordRecoveryToken.objects.get_or_create(owner=user,
                                                                      defaults={'creation': now(),
                                                                                'token': 'non_generated_token'})

            # Update creation date and used status, also set a token value
            if not create:
                rec.used = False

            rec.set_token()
            rec.creation = now()
            rec.save()

            link = 'https://app.einsteinfloripa.com.br/sistema-interno/#/recovery/'
            data = {
                'NAME': user.first_name,
                'LINK': link + rec.token,
                'VALID_HOURS': settings.RECOVERY_VALID_HOURS,
            }

            template = get_template('email/mail_recovery.html')
            html = template.render(data)

            # Send e-mail
            mail = EmailMessage(
                'Recuperação de senha', html,
                '"Einstein Floripa" <naoresponda@einsteinfloripa.com.br>',
                [email],
            )

            mail.content_subtype = 'html'
            mail.send()

        return Response({'done': True}, status=status.HTTP_200_OK)


class PasswordReset(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        fail_response = Response({'done': False,
                                  'detail': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)

        token = request.data['token']
        exists = PasswordRecoveryToken.objects.filter(token=token).count() == 1

        if exists:
            rec = PasswordRecoveryToken.objects.get(token=token)
            delta = now() - rec.creation
            if delta.total_seconds()/3600 > settings.RECOVERY_VALID_HOURS or rec.used:
                return fail_response

            password = request.data['password']
            user = rec.owner

            user.set_password(password)
            user.save()

            rec.used = True
            rec.save()

            return Response({'done': True,
                             'detail': 'Password changed'}, status=status.HTTP_200_OK)

        else:
            return fail_response


class UserInformation(APIView):
    def get(self, request):
        user = User.objects.get(email=request._user)
        serial_user = UserSerializer(user).data
        serial_user['full_name'] = user.full_name
        serial_user.pop('password', None)

        return Response(serial_user, status=status.HTTP_200_OK)


class UserResource(APIView):
    def get(self, request):
        user = User.objects.get(email=request._user)
        resources = Resource.objects.filter(owner=user)
        serial_resources = ResourceSerializer(resources, many=True).data

        return Response(serial_resources, status=status.HTTP_200_OK)


class ContactsInformation(APIView):
    def get(self, request, label):
        users = User.objects.filter(ocupation__label=label, is_active=True)
        info = [
            {'name': u.full_name,
             'contact': u.main_contact,
             # 'role': u.role.description
             }
            for u in users]
        return Response(info, status=status.HTTP_200_OK)


class ClassSchedules(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, class_label):
        try:
            class_room = ClassRoom.objects.get(label=class_label)
        except:
            return Response({'done': False,
                             'detail': 'class label not found'},
                            status=status.HTTP_400_BAD_REQUEST)

        for block in request.data:
            try:
                time_delta = block['time']
                time = datetime.datetime.strptime(
                    time_delta.split(' - ')[0], '%Hh%M').time()

                days = list(block.keys())
                days.remove('time')

                subject_list = ClassSubject.objects.all()

                for day in days:
                    class_time = ClassTime.objects.get(
                        week_day=day[:3], init_time=time)
                    registers = ClassRegister.objects.filter(
                        classroom=class_room, class_time=class_time)

                    # Deleting and adding new registers because of the possibility of multiple
                    # subjects on same time and class room
                    # Delete previous registers for the class and time
                    for reg in registers:
                        reg.delete()

                    # Create new registers
                    for subject in block[day]:
                        for sub in subject_list:
                            if subject == sub.verbose:
                                reg = ClassRegister(classroom=class_room,
                                                    class_time=class_time,
                                                    subject=sub)
                                reg.save()
            except:
                raise
                return Response({'done': False,
                                 'detail': f'failed in block `{block}`'},
                                status=status.HTTP_400_BAD_REQUEST)

        return Response({'done': True, 'detail': 'class registers modified'},
                        status=status.HTTP_200_OK)

    def get(self, request, class_label=None):
        if class_label is None:
            classes = ClassRoom.objects.filter(year=timezone.now().year)
            labels = [{'name': c.name,
                       'label': c.label
                       } for c in classes]

            return Response(labels, status=status.HTTP_200_OK)

        times = ClassRegister.unique_times(class_label)
        pairs = [
            ('mon', 'monday'),
            ('tue', 'tuesday'),
            ('wed', 'wednesday'),
            ('thu', 'thursday'),
            ('fri', 'friday'),
        ]

        ret = []
        for (begin, end) in times:
            d = dict()
            d['time'] = f'{begin.hour}h{begin.minute} - {end.hour}h{end.minute}'
            for p in pairs:
                classes = ClassRegister.objects.filter(
                    classroom__label=class_label,
                    class_time__init_time=begin,
                    class_time__week_day=p[0]
                )

                c = [cl.subject_label for cl in classes]
                d[p[1]] = c

            ret.append(d)

        return Response(ret, status=status.HTTP_200_OK)


class OccurenceOperations(APIView):
    def post(self, request):
        # Limit to embaixada
        accessing_user = User.objects.get(email=request._user)
        if accessing_user.ocupation.label not in ['MTX', 'EMB']:
            return Response({'done': False, 'detail': 'Unauthorized user'},
                            status=status.HTTP_403_FORBIDDEN)

        try:
            action = request.data['action']
            occur_id = request.data['id']
        except KeyError:
            return Response({'done': False,
                             'detail': 'Missing parameters'},
                            status=status.HTTP_400_BAD_REQUEST)

        # Actions settings
        actions = {'toggle-solved': self.toggle_solved}

        if action not in actions.keys():
            return Response({'done': False,
                             'detail': f'invalid action. Valid actions: {actions}'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            occur = Occurrences.objects.get(id=occur_id)

        except ObjectDoesNotExist:
            return Response({'done': False,
                             'detail': 'invalid id.'},
                            status=status.HTTP_400_BAD_REQUEST)

        # Call function to make the action
        actions[action](occur_id)

        return Response({'done': True,
                         'detail': ''}, status=status.HTTP_200_OK)

    def toggle_solved(self, occur_id):
        occur = Occurrences.objects.get(id=occur_id)
        occur.solved = not occur.solved
        occur.save()


class UnsolvedOccurrenceView(APIView):
    def get(self, request):
        # Limit to embaixada
        accessing_user = User.objects.get(email=request._user)
        if accessing_user.ocupation.label not in ['MTX', 'EMB']:
            return Response({'done': False, 'detail': 'Unauthorized user'},
                            status=status.HTTP_403_FORBIDDEN)

        occurences = Occurrences.objects.filter(solved=False)

        ret = list()
        for occur in occurences:
            anon = occur.anonymous
            creator = occur.creator
            ret.append({
                'id': occur.id,
                'student_name': None if anon else creator.full_name,
                'student_mail':  None if anon else creator.email,
                'student_phone': None if anon else creator.main_contact,
                'content': occur.content,
                'title': occur.title,
                'solved': occur.solved,
                'creation': occur.creation,
                'modified': occur.modified,
                'type': occur.type.label,
                'type_id': occur.type.id
            })

        return Response(ret, status=status.HTTP_200_OK)


class SolvedOccurrenceView(APIView):
    def get(self, request, num_occur=10):
        # Limit to embaixada
        accessing_user = User.objects.get(email=request._user)
        if accessing_user.ocupation.label not in ['MTX', 'EMB']:
            return Response({'done': False, 'detail': 'Unauthorized user'},
                            status=status.HTTP_403_FORBIDDEN)

        occurences = Occurrences.objects.filter(solved=True)

        if len(occurences) > num_occur:
            occurences = occurences[0:num_occur]

        ret = list()
        for occur in occurences:
            anon = occur.anonymous
            creator = occur.creator
            ret.append({
                'id': occur.id,
                'student_name': None if anon else creator.full_name,
                'student_mail':  None if anon else creator.email,
                'student_phone': None if anon else creator.main_contact,
                'content': occur.content,
                'title': occur.title,
                'solved': occur.solved,
                'creation': occur.creation,
                'modified': occur.modified,
                'type': occur.type.label,
                'type_id': occur.type.id
            })

        return Response(ret, status=status.HTTP_200_OK)


class OccurrenceCreationView(APIView):
    def post(self, request):
        try:
            anon = request.data['anonymous']
            title = request.data['title']
            content = request.data['content']
            type_id = request.data['type_id']

        except KeyError:
            return Response({'done': False,
                             'detail': 'Parameters not found'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            occur_type = OccurrenceType.objects.get(id=type_id)

        except ObjectDoesNotExist:
            return Response({'done': False,
                             'detail': 'type id not found'},
                            status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(email=request._user)
        occur = Occurrences(creator=user,
                            type=occur_type,
                            title=title,
                            content=content,
                            solved=False,
                            anonymous=anon)

        occur.save()

        return Response({'done': True, 'detail': 'Occurence registered'},
                        status=status.HTTP_200_OK)


class ProcessView(APIView):
    def get(self, request, dpt):

        process = Process.objects.filter(dpt=dpt)

        ret = list()
        for process in process:
            ret.append({
                'id': process.id,
                'content': process.content,
                'title': process.title,
                'url': process.url,
            })

        return Response(ret, status=status.HTTP_200_OK)


class ProcessCreationView(APIView):
    def post(self, request):
        try:
            title = request.data['title']
            content = request.data['content']
            url = request.data['url']
            dpt = request.data['dpt']

        except KeyError:
            return Response({'done': False, 'detail': 'Parameters not found'},
                            status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(email=request._user)

        proc = Process(creator=user,
                       title=title,
                       content=content,
                       url=url,
                       dpt=dpt,)

        proc.save()

        return Response({'done': True, 'detail': 'Process registered'},
                        status=status.HTTP_200_OK)

    def delete(self, request):
        try:
            proc_id = request.data['id']

        except KeyError:
            return Response({'done': False,
                             'detail': 'invalid form'},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            proc = Process.objects.get(id=proc_id)

        except:
            return Response({'done': False,
                             'detail': 'invalid id'},
                            status=status.HTTP_400_BAD_REQUEST)

        proc.delete()

        return Response({'done': True,
                         'detail': 'test deleted'},
                        status=status.HTTP_200_OK)


class TestCreationView(APIView):
    def post(self, request):
        try:
            dt_format = '%Y-%m-%d %H:%M:%S'
            if 'id' in request.data.keys():
                test_id = request.data['id']

            else:
                test_id = None

            opened = request.data['open']
            label = request.data['label']
            open_sub = datetime.datetime.strptime(
                request.data['open_sub'], dt_format)
            close_sub = datetime.datetime.strptime(
                request.data['close_sub'], dt_format)
            test_datetime = datetime.datetime.strptime(
                request.data['test_datetime'], dt_format)

        except KeyError:
            return Response({'done': False,
                             'detail': 'invalid form'},
                            status=status.HTTP_400_BAD_REQUEST)

        if test_id is None:
            test = Tests(label=label,
                         open_sub=open_sub,
                         close_sub=close_sub,
                         open=opened,
                         test_datetime=test_datetime)
            detail = 'test created'

        else:
            test = Tests.objects.get(id=test_id)
            test.label = label
            test.open_sub = open_sub
            test.close_sub = close_sub
            test.open = opened
            test.test_datetime = test_datetime
            detail = 'test modified'

        test.save()

        return Response({'done': True,
                         'detail': detail},
                        status=status.HTTP_200_OK)

    def delete(self, request):
        try:
            test_id = request.data['id']

        except KeyError:
            return Response({'done': False,
                             'detail': 'invalid form'},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            test = Tests.objects.get(id=test_id)

        except:
            return Response({'done': False,
                             'detail': 'invalid id'},
                            status=status.HTTP_400_BAD_REQUEST)

        subs = TestsSubscriptions.objects.filter(test=test)
        for sub in subs:
            sub.delete()

        test.delete()

        return Response({'done': True,
                         'detail': 'test deleted'},
                        status=status.HTTP_200_OK)


class SubjectsView(APIView):
    def get(self, request):
        objects = ClassSubject.objects.all()
        classes = [o.verbose for o in objects]

        return Response(classes, status=status.HTTP_200_OK)
