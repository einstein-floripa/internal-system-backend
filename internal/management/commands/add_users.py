from django.core.management.base import BaseCommand
from loguru import logger

import csv
import os
from datetime import datetime

from internal.models import User, Ocupation, Gender


class Command(BaseCommand):
    date_format = '%d-%m-%Y'
    help = 'Add users from a csv file.'
    data = {
        'first_name': '<FIRST_NAME - string>',
        'last_name': '<LAST_NAME - string>',
        'register': '<REGISTER_CODE - string>',
        'cpf': '<CPF - string (NO DOTS, 11 CHARACTERS)>',
        'type': None,
        'address': '<ADDRESS - string>',
        'email': '<EMAIL_ADDRESS - string >',
        'phone': f'<PHONE_NUMBER - string, numbers only>',
        'birthday': '<BITRH_DAY - string in ' + date_format + '>',
        'ocupation': '<CHECK_AVAILABLES_ON_DB_BY_LABEL - string>',
        'gender': '<CHECK_AVAILABLES_ON_DB_BY_LABEL - string>',
    }

    def add_arguments(self, parser):
        # Argument to receive
        parser.add_argument('--csv_file',
                            type=str,
                            help='File with users. --template for genrating template with column names')

        # Argumemnt to generate template of csv file
        parser.add_argument('--template',
                            action='store_true',
                            help='Generate template of csv file')
        parser.add_argument('--output',
                            type=str,
                            help='Output filename')

    def generate_template_file(self, filename):
        with open(filename, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=self.data.keys())
            writer.writeheader()
            writer.writerow(self.data)

    def save_users(self, filename):
        with open(filename) as f:
            reader = csv.DictReader(f)

            for row in reader:
                u = User(
                    first_name=row['first_name'],
                    last_name=row['last_name'],
                    register=row['register'],
                    cpf=row['cpf'],
                    address=row['address'],
                    email=row['email'],
                    phone=row['phone'],
                    main_contact=row['phone'],
                    type=None,
                    birthday=datetime.strptime(
                        row['birthday'], self.date_format),
                    gender=Gender.objects.get(label=row['gender']),
                    ocupation=Ocupation.objects.get(label=row['ocupation']),
                )

                u.set_password(User.objects.make_random_password())

                u.save()

    def validate_archive(self, filename):
        with open(filename) as f:
            reader = csv.DictReader(f)
            line = 0
            for row in reader:
                # Validate first_name length
                max_len = User._meta.get_field('first_name').max_length
                if len(row['first_name']) > max_len:
                    return (
                        False,
                        f'first_name field have a max_length of {max_len}, ' +
                        f'exceeded on {row["first_name"]} on line {line}'
                    )

                # Validate last_name length
                max_len = User._meta.get_field('last_name').max_length
                if len(row['last_name']) > max_len:
                    return (
                        False,
                        f'last_name field have a max_length of {max_len}, ' +
                        f'exceeded on {row["last_name"]} on line {line}'
                    )

                # Validate register length
                max_len = User._meta.get_field('register').max_length
                if len(row['register']) > max_len:
                    return (
                        False,
                        f'register field have a max_length of {max_len}, ' +
                        f'exceeded on {row["register"]} on line {line}'
                    )

                # Validate cpf length and characters
                max_len = User._meta.get_field('cpf').max_length
                if len(row['cpf']) != max_len:
                    return (
                        False,
                        f'cpf field have a exact len of {max_len}, ' +
                        f'wrong on {row["cpf"]} on line {line}'
                    )

                if not row['cpf'].isalnum():
                    return (
                        False,
                        f'cpf field have to be only numbers, ' +
                        f'wrong on {row["cpf"]} on line {line}'
                    )

                # validate email length
                max_len = User._meta.get_field('email').max_length
                if len(row['email']) > max_len:
                    return (
                        False,
                        f'email field have a max len of {max_len}, ' +
                        f'wrong on {row["email"]} on line {line}'
                    )

                # validate phone length and characters
                max_len = User._meta.get_field('phone').max_length
                if len(row['phone']) > max_len:
                    return (
                        False,
                        f'phone field have a max len of {max_len}, ' +
                        f'wrong on {row["phone"]} on line {line}'
                    )

                if not row['phone'].isalnum():
                    return (
                        False,
                        f'phone field have to be only numbers, ' +
                        f'wrong on {row["phone"]} on line {line}'
                    )

                # validate date of birthday
                try:
                    datetime.strptime(row['birthday'], self.date_format)
                except ValueError:
                    return (
                        False,
                        f'birthday field have to be on format {self.date_format}, ' +
                        f'wrong on {row["birthday"]} on line {line}'
                    )

                # validate ocupation
                pos = [None] + [i.label for i in Ocupation.objects.all()]
                if row['ocupation'] not in pos:
                    return (
                        False,
                        f'ocupation have to be in the following possibilitiess:' +
                        str(pos) +
                        f' wrong on {row["ocupation"]} on line {line}'
                    )

                # validate gender
                pos = [None] + [i.label for i in Gender.objects.all()]
                if row['gender'] not in pos:
                    return (
                        False,
                        f'gender have to be in the following possibilitiess:' +
                        str(pos) +
                        f' wrong on {row["gender"]} on line {line}'
                    )

                line += 1

        return (True, 'Validation successful')

    def handle(self, *args, **options):
        if options['template']:
            # Grant that if a template is already in the folder,
            # the script creates a new one, don't messing with a
            # possible template with data in
            filename = 'add_user_template.csv'
            while True:
                if os.path.isfile(filename):
                    filename = 'new_' + filename
                else:
                    break

            # Set filename to argument, if it have
            if options['output']:
                filename = options['output']

            logger.info(f'Generating template file as `{filename}`')
            self.generate_template_file(filename)
            logger.info('File generated!')

        elif options['csv_file']:
            if not os.path.isfile(options['csv_file']):
                logger.error(f'File `{options["csv_file"]}` does not exist!`')
                return

            valid, message = self.validate_archive(options['csv_file'])
            if valid:
                logger.info(message)
                logger.info('Saving users')
                self.save_users(options['csv_file'])
                logger.info('Users saved!')
            else:
                logger.error(message)

        else:
            self.print_help(__file__, '')
