from datetime import time, timedelta, date, datetime
import json

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import viewsets
from django.utils import timezone
from django.conf import settings
from pandas import DataFrame, Series, concat

from .models import Attendance, AttendanceStates
from .serializers import StatesSerializer
from internal.models import Ocupation, User


class StatesViewSet(viewsets.ModelViewSet):
    queryset = AttendanceStates.objects.all()
    serializer_class = StatesSerializer


class SearchStudentView(APIView):
    def get(self, request, user_register):
        try:
            user = User.objects.get(register=user_register)
            if Ocupation.objects.get(id=user.ocupation_id).label == 'STU':

                return Response({'done': True,
                                 'name': user.full_name,
                                 }, status=status.HTTP_200_OK)
            else:
                raise

        except:
            return Response({'done': False,
                             'detail': 'student not found'
                             }, status=status.HTTP_400_BAD_REQUEST)


class RegisterView(APIView):
    def post(self, request):
        user = User.objects.get(email=request._user)
        try:
            id_state = request.data['id_state']
            code = request.data['code']
            action_timestamp = request.data['action_timestamp']
        except:
            return Response({'done': False,
                             'detail': 'state id or code not found'},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            student = User.objects.get(register=code)
        except:
            return Response({'done': False,
                             'detail': 'student not found'},
                            status=status.HTTP_400_BAD_REQUEST)

        reg = Attendance(register_by=user,
                         student=student,
                         state=AttendanceStates.objects.get(id=id_state),
                         action_timestamp=action_timestamp)

        reg.save()

        return Response({'done': True,
                         'name': student.full_name},
                         status=status.HTTP_200_OK)
        

class AttendanceTableView(APIView):
    def get(self, request, month, year=None):
        accessing_user = User.objects.get(email=request._user)
        if accessing_user.ocupation.label not in ['MTX', 'EMB']:
            return Response({'done': False, 'detail': 'Unauthorized user'},
                            status=status.HTTP_403_FORBIDDEN)

        # year variable for possible future upgrades on function
        if year is None:
            year = timezone.now().year
            
        states = dict()
        for state in AttendanceStates.objects.all():
            states[state.id] = state.description

        # Load attendances from DB
        attendances = Attendance.objects.filter(action_timestamp__month=month,
                                                action_timestamp__year=year)
        # PUt the into a dataframe. 
        df = DataFrame(list(attendances.values()))

        # Create columns for date and time
        df['day'] = df['action_timestamp'].dt.date
        df['time'] = df['action_timestamp'].dt.time

        class_days = df['day'].unique()
        class_days.sort()

        class_begin = list()
        class_begin = list()
        for i in range(settings.CLASS_COUNT):
            t = self.sum_time_timedelta(settings.BEGIN_OF_FIRST_CLASS,
                                        i * settings.CLASS_DURATION)

            # this will be 0 or 1, just to sum interval time to class begin             
            interval_time = int(i / settings.CLASSES_BEFORE_INTERVAL) * \
                            settings.CLASS_INTERVAL_DURATION
            c = self.sum_time_timedelta(t, interval_time)
            class_begin.append(c)

        info = DataFrame(index=df['student_id'].unique())

        for day in class_days:
            # Select a day for work on
            day_info = df.loc[df['day'] == day]
            # Drop duplicate rows, keeping the last register
            day_info = day_info.drop_duplicates(subset=['student_id', 'day', 'state_id'], keep='last')

            d_info = DataFrame(index=info.index)
            for i in range(1, 6):
                values = day_info.loc[day_info['state_id'] == i]
                values.set_index('student_id', inplace=True)
                index = values.index

                d_info.loc[index, states[i]] = values['time']

            d_info['presence'] = ~d_info[states[2]].isna() * settings.CLASS_COUNT
            d_info['presence'] += ~d_info[states[3]].isna() * settings.CLASS_COUNT
            # Substrai uma presença pra cada início de aula perdido pela 
            for value in class_begin:
                d_info['presence'] -= d_info['Entrada Atrasada'] > value

            # Subtrai uma presença pra cada início de aula perdido 
            for value in reversed(class_begin):
                d_info['presence'] -= d_info['Saída Adiantada'] < value
                
            info[day] = d_info['presence']
        
        # Put student_id as column to convert to student name
        info = info.reset_index()
        info['name'] = info['index'].apply(self.make_users_column)

        for day in class_days:
            info[day.strftime("%d/%m/%Y")] = info[day]
            info.drop([day], axis='columns', inplace=True)

        info['attendance_pctg'] = info.iloc[:, 1:].sum(axis=1) / (settings.CLASS_COUNT * len(class_days))
        info['problem'] = info['attendance_pctg'] < 0.75

        # Append active users that don't show up in any class
        extras = User.objects.filter(is_active=True, ocupation__label='STU')
        lines = []
        for stu in extras:
            if stu.id not in info['index']:
                line = [stu.full_name, stu.id, *([0] * len(class_days)), True, 0]
                lines.append(line)
        
        columns = ['name', 'index',
                   *[day.strftime("%d/%m/%Y") for day in class_days],
                   'problem', 'attendance_pctg']
        extras_df = DataFrame(lines, columns=columns)

        info = info.append(extras_df, sort=True)

        dicts_in_list = json.loads(info.to_json(orient='records'))

        # Format to put register code as key for student data
        ret = dict()
        for d in dicts_in_list:
            identifier = d['index']
            u = User.objects.get(id=identifier)
            d.pop('index', None)
            ret[u.register] = d

        return Response(ret)


    def sum_time_timedelta(self, t : time, td : timedelta):
        dt = datetime.combine(date.today(), t) + td
        return dt.time()

    def make_users_column(self, student_id):
        u = User.objects.get(id=student_id)
        return u.full_name



        

    
