from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.db import models
from django.conf import settings
import pytz
from datetime import timedelta, datetime

class ExpiringTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        try:
            token = self.get_model().objects.get(key=key)
        except models.ObjectDoesNotExist:
            raise AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise AuthenticationFailed('User inactive or deleted')

        # This is required for the time comparison
        utc_now = datetime.utcnow()
        utc_now = utc_now.replace(tzinfo=pytz.utc)

        valid_hours = timedelta(hours=settings.TOKEN_VALID_HOURS)
        if token.created < utc_now - valid_hours:
            raise AuthenticationFailed('Token has expired')

        return token.user, token
