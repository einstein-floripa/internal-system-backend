# Inicializando
1. Instalar python [Site](python.org)
2. Instalar pip [get-pip](https://pip.pypa.io/en/stable/installing/)
3. Instalar [pipenv](https://pipenv.readthedocs.io/en/latest/)
4. Executar ```pipenv install -e .``` 
5. Executar ```pipenv shell`` para entrar na virtualenv

A partir daqui, você está no ambiente de desenvolvimento, e poderá executar os comandos Django.