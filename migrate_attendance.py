import os

import MySQLdb
import django

# Set up django environment
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "internal_system_backend.settings")
django.setup()

from frequence_control.models import Attendance, AttendanceStates
from internal.models import User


DB = 'einst301_automation'
DB_HOST = '216.172.172.234'
DB_USER = 'einst301_matrix'
DB_PASSWORD = 'M3lh0rD3p4rt4m3nt0' #nosec

db = MySQLdb.connect(
            db=DB,
            host=DB_HOST,
            user=DB_USER,
            password=DB_PASSWORD,
            use_unicode=True,
        )

def fetch_db(sql, db):
    cursor = db.cursor()

    cursor.execute(sql)
    fields = [i[0] for i in cursor.description]
    data = [dict(zip(fields, student)) for student in cursor.fetchall()]

    return data
          

def run():
    # Get user to hold every register
    u = User.objects.get(cpf='09011837924')

    sql = 'SELECT * FROM attendance WHERE NOT added'
    attendances = fetch_db(sql, db)

    for attendance in attendances:
        try:
            student = User.objects.get(register=attendance['code'])
        except:
            print(f'Matrícula {attendance["code"]} not found.')
            continue

        state = AttendanceStates.objects.get(id=attendance['id_state'])

        at = Attendance(register_by=u,
                        student=student,
                        state=state,
                        action_timestamp=attendance['action_timestamp']
                    )

        at.save()
        cursor = db.cursor()
        cursor.execute(f'UPDATE attendance SET added=1 WHERE id={attendance["id"]}')
        db.commit()

        print(f'Register of {at.__str__()}')


    

if __name__ == '__main__':
    run()