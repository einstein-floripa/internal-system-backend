import datetime
import binascii
import os

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


# model UserType
class UserType(models.Model):
    label = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.label


class Ocupation(models.Model):
    label = models.CharField(max_length=3)
    description = models.CharField(max_length=50)

    def __str__(self):
        return str(self.id) + ' - ' + self.description


class Gender(models.Model):
    label = models.CharField(max_length=50)

    def __str__(self):
        return self.label


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class University(models.Model):
    label = models.CharField(max_length=20)

    def __str__(self):
        return self.label


class Course(models.Model):
    university = models.ForeignKey(University, on_delete=models.PROTECT)
    label = models.CharField(max_length=150)
    code = models.CharField(max_length=30)

    def __str__(self):
        return self.label


class Role(models.Model):
    description = models.CharField(max_length=80)

    def __str__(self):
        return self.description

class User(AbstractBaseUser):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, null=True)
    register = models.CharField(max_length=8, default='00000000', blank=True)
    cpf = models.CharField(max_length=11)
    phone = models.CharField(max_length=11)
    main_contact = models.CharField(max_length=254, null=True)

    address = models.TextField()

    email = models.EmailField(max_length=254, unique=True)

    register_day = models.DateField(auto_now_add=True)
    birthday = models.DateField(null=True)

    ocupation = models.ForeignKey(Ocupation, on_delete=models.PROTECT, null=True)
    gender = models.ForeignKey(Gender, on_delete=models.PROTECT, null=True)
    type = models.ForeignKey(UserType, on_delete=models.PROTECT, null=True)
    university = models.ForeignKey(University, on_delete=models.PROTECT, null=True, blank=True)
    course = models.ForeignKey(Course, on_delete=models.PROTECT, null=True, blank=True)
    role = models.ForeignKey(Role, on_delete=models.PROTECT, null=True, blank=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name



class Tests(models.Model):
    label = models.CharField(max_length=50)
    open_sub = models.DateTimeField()
    close_sub = models.DateTimeField()
    open = models.BooleanField(default=True)
    test_datetime = models.DateTimeField()

    def __str__(self):
        return f'{self.label}  {self.test_datetime.strftime("%d/%m/%Y")}'


class Quota(models.Model):
    code = models.CharField(max_length=30)
    descriptive_text = models.TextField()

    def __str__(self):
        return self.descriptive_text


class Language(models.Model):
    label = models.CharField(max_length=30)
    descriptive_text = models.TextField()

    def __str__(self):
        return self.descriptive_text


class TestsSubscriptions(models.Model):
    test = models.ForeignKey(Tests, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    quota = models.ForeignKey(Quota, on_delete=models.PROTECT)
    course = models.ForeignKey(Course, on_delete=models.PROTECT)

    class Meta():
        unique_together = ('test', 'user',)

    def __str__(self):
        return self.user.first_name + ' - ' + self.test.label


class TestsAttendance(models.Model):
    label = models.CharField(max_length=50)

    def __str__(self):
        return self.label


class TestsResults(models.Model):
    sub_id = models.ForeignKey(TestsSubscriptions, on_delete=models.PROTECT)
    attended = models.ForeignKey(TestsAttendance, on_delete=models.PROTECT)
    feedback = models.TextField()

class PasswordRecoveryToken(models.Model):
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    creation = models.DateTimeField()

    token = models.CharField(max_length=40)
    used = models.BooleanField(default=False)

    def set_token(self):
        self.token = self.generate_key()

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.owner.email


class Resource(models.Model):
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    description = models.TextField()
    link = models.TextField()

    str_info = models.CharField(max_length=150)

    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.str_info
    
class ClassTime(models.Model):
    init_time = models.TimeField()
    
    week_day_choices = [
        ('mon', 'Segunda-feira'),
        ('tue', 'Terça-feira'),
        ('wed', 'Quarta-feira'),
        ('thu', 'Quinta-feira'),
        ('fri', 'Sexta-Feira'),
        ('sat', 'Sábado')
    ]
    week_day = models.CharField(
        max_length=3,
        choices=week_day_choices
    )

    class_duration = models.IntegerField(
        help_text='Class duration in minutes'
    )

    def __str__(self):
        return self.get_week_day_display() + ' at ' + \
               self.init_time.strftime('%H:%M')

    @property
    def end_time(self):
        t = timezone.datetime.combine(timezone.now(), self.init_time)
        return (t + timezone.timedelta(minutes=self.class_duration)).time()

    class Meta:
        unique_together = ['week_day', 'init_time']


class ClassSubject(models.Model):
    label = models.CharField(max_length=30)
    divisions = [
        ('u', 'Frente única'),
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    ]
    division = models.CharField(
        max_length=1,
        choices=divisions
    )

    @property
    def verbose(self):
        if self.division == 'u':
            return f'{self.label}'

        else:
            return str(self)

    def __str__(self):
        return f'{self.label} {self.get_division_display()}'

class ClassRoom(models.Model):
    label = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    year = models.IntegerField()

    def __str__(self):
        return f'{self.label} {self.name} - {self.year}'


class StudentClassRoom(models.Model):
    student = models.ForeignKey(User, on_delete=models.PROTECT)
    class_room = models.ForeignKey(ClassRoom, on_delete=models.PROTECT)

    def __str__(self):
        return self.student.first_name + ' ' + self.class_room.name


class ClassRegister(models.Model):
    classroom = models.ForeignKey(ClassRoom, on_delete=models.PROTECT)
    class_time = models.ForeignKey(ClassTime, on_delete=models.PROTECT)
    subject = models.ForeignKey(ClassSubject, on_delete=models.PROTECT)


    def __str__(self):
        return self.classroom.label + ', na ' + \
               self.class_time.get_week_day_display() + ' as ' + \
               self.class_time.init_time.strftime('%H:%M') + ' - ' + \
               self.subject.label + ' ' + self.subject.get_division_display()

    @property
    def subject_label(self):
        return self.subject.verbose

    @staticmethod
    def unique_times(class_label):
        classes = ClassRegister.objects.filter(classroom__label=class_label)
        ret = list(set(([(c.class_time.init_time, c.class_time.end_time)
                          for c in classes])))
        ret.sort()
        return ret


class OccurrenceType(models.Model):
    label = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.label


class Occurrences(models.Model):
    creator = models.ForeignKey(User, on_delete=models.PROTECT)
    type = models.ForeignKey(OccurrenceType, on_delete=models.PROTECT)

    title = models.CharField(max_length=100)
    content = models.TextField()
    
    solved = models.BooleanField(default=False)
    anonymous = models.BooleanField()

    creation = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    def __str__(self):
        return self.creator.full_name + ' - ' + self.type.label

class Process(models.Model):
    creator = models.ForeignKey(User, on_delete=models.PROTECT)

    title = models.CharField(max_length=100)
    content = models.TextField()
    url = models.TextField()

    dpt = models.TextField()


    creation = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    def __str__(self):
        return self.title