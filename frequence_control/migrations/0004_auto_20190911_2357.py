# Generated by Django 2.2.5 on 2019-09-11 23:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frequence_control', '0003_auto_20190905_2235'),
    ]

    operations = [
        migrations.RenameField(
            model_name='attendance',
            old_name='register_timestamps',
            new_name='register_timestamp',
        ),
    ]
