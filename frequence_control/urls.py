from django.urls import path
from django.conf.urls import url

from . import views


urlpatterns = [
    path('states/', views.StatesViewSet.as_view({'get': 'list'})),
    path('search/<user_register>', views.SearchStudentView.as_view()),
    path('register/', views.RegisterView.as_view()),
    path('table/<month>', views.AttendanceTableView.as_view())
]
