# Generated by Django 2.2.4 on 2019-08-27 19:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frequence_control', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='attendance',
            old_name='register_by_id',
            new_name='register_by',
        ),
        migrations.RenameField(
            model_name='attendance',
            old_name='state_id',
            new_name='state',
        ),
        migrations.RenameField(
            model_name='attendance',
            old_name='student_id',
            new_name='student',
        ),
    ]
