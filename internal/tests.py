from django.test import TestCase
from rest_framework.test import APIClient
from django.utils import timezone
from .models import *


class AuthenticationTest(TestCase):
    def setUp(self):
        o = Ocupation(label='STU', description='Alunos e Alunas')
        o.save()

        t = UserType(label='STU', description='Alunos e Alunas')
        t.save()

        self.username = 'test@test.test'
        self.password = 'test@password'

        u = User.objects.create_user(
            email=self.username,
            password=self.password
        )

        u.first_name='Johnny'
        u.last_name='Test'
        u.cpf='12345678909'
        u.phone='48987654321'
        u.main_contact='48987654321'
        u.address = 'Nowhere'
        u.ocupation = o
        u.type = t
        
        u.save()

    def test_login(self):
        c = APIClient()

        r = c.post('/internal/api-token-auth/',
                   {'username': self.username, 'password': self.password})

        self.assertIn('token', r.json().keys())


class TestSubscription(TestCase):
    def setUp(self):
        # Setup user
        o = Ocupation(label='STU', description='Alunos e Alunas')
        o.save()
        
        t = UserType(label='STU', description='Alunos e Alunas')
        t.save()

        self.username = 'test@test.test'
        self.password = 'test@password'

        u = User.objects.create_user(
            email=self.username,
            password=self.password
        )

        u.first_name='Johnny'
        u.last_name='Test'
        u.cpf='12345678909'
        u.phone='48987654321'
        u.main_contact='48987654321'
        u.address = 'Nowhere'
        u.ocupation = o
        u.type = t
        
        u.save()

        # Setup tests
        now = timezone.now()
        # Can subscribe
        t = Tests(label='Simuteste 1',
                  open_sub=now - timezone.timedelta(hours=3),
                  close_sub=now + timezone.timedelta(hours=2),
                  test_datetime=now + timezone.timedelta(days=2)
                )
        
        # Can't subscribe
        t = Tests(label='Simuteste 2',
                  open_sub=now - timezone.timedelta(hours=3),
                  close_sub=now + timezone.timedelta(hours=2),
                  test_datetime=now + timezone.timedelta(days=2),
                  open=False
                )

        # Closed
        t = Tests(label='Simuteste 3',
                  open_sub=now - timezone.timedelta(hours=3),
                  close_sub=now - timezone.timedelta(hours=2),
                  test_datetime=now + timezone.timedelta(days=2)
                )
        
        # Passed
        t = Tests(label='Simuteste 4',
                  open_sub=now - timezone.timedelta(days=4, hours=3),
                  close_sub=now - timezone.timedelta(days=3, hours=2),
                  test_datetime=now - timezone.timedelta(days=2)
                )

        # TODO register course, language and quota for testing


    def test_display(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username, 'password': self.password})
        r = r.json()

        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        ret = c.get('/internal/tests/info-and-subs/')

        json_ret = ret.json()

        # TODO test for each case
        # Try to subscribe on closed and passed, get denied
        # Try to subscribe on open, get OK and check fi register
        # was done right.

class TestOccurences(TestCase):
    def setUp(self):
        # Setup user for student
        o_stu = Ocupation(label='STU', description='Alunos e Alunas')
        o_stu.save()

        
        t_stu = UserType(label='STU', description='Alunos e Alunas')
        t_stu.save()

        self.username_johnny = 'johnny@test.test'
        self.password_johnny = 'johnny@password'

        u_stu = User.objects.create_user(
            email=self.username_johnny,
            password=self.password_johnny
        )

        u_stu.first_name='Johnny'
        u_stu.last_name='Test'
        u_stu.cpf='12345678909'
        u_stu.phone='48987654321'
        u_stu.main_contact='48987654321'
        u_stu.address = 'Nowhere'
        u_stu.ocupation = o_stu
        u_stu.type = t_stu
        u_stu.save()

        # User for Embaixada do Amor
        o_emb = Ocupation(label='EMB', description='Embaixada do Amor')
        o_emb.save()

        t_emb = UserType(label='Organizador(a)', description='Organizadores')
        t_emb.save()

        self.username_emb = 'embaixadora@test.test'
        self.password_emb = 'embaixadora@password'
        u_emb = User.objects.create_user(
            email=self.username_emb,
            password=self.password_emb
        )

        u_emb.first_name='Embaixadora'
        u_emb.last_name='Amorosa'
        u_emb.cpf='12345678918'
        u_emb.phone='48987654321'
        u_emb.main_contact='48987654321'
        u_emb.address = 'Nowhere'
        u_emb.ocupation = o_emb
        u_emb.type = t_emb
        u_emb.save()

        # Create occurence type
        occur_type = OccurrenceType(label='lbl', description='desc')
        occur_type.save()

        # Create one occurrence, that will be id=1
        occur1 = Occurrences(creator=u_stu,
                             type=occur_type,
                             title='Test',
                             content='Testando esse role',
                             anonymous=False)
        occur1.save()

        # Create one occurrence, that will be id=2
        occur2 = Occurrences(creator=u_stu,
                             type=occur_type,
                             title='Test',
                             content='Testando esse role anonimamente',
                             anonymous=False,
                             solved=True)
        occur2.save()
    
    def test_embaixada_only(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_johnny, 'password': self.password_johnny})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        ret = c.get('/internal/occurrences/solveds/')

        json_ret = ret.json()

        self.assertEqual(ret.status_code, 403)
        self.assertFalse(json_ret['done'])

        ret = c.get('/internal/occurrences/unsolveds/')
        json_ret = ret.json()

        self.assertEqual(ret.status_code, 403)
        self.assertFalse(json_ret['done'])

    def test_solved_list(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_emb, 'password': self.password_emb})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        ret = c.get('/internal/occurrences/solveds/')

        json_ret = ret.json()

        self.assertEqual(ret.status_code, 200)
        self.assertEqual(len(json_ret), 1)

    def test_unsolved_list(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_emb, 'password': self.password_emb})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        ret = c.get('/internal/occurrences/unsolveds/')

        json_ret = ret.json()

        self.assertEqual(ret.status_code, 200)
        self.assertEqual(len(json_ret), 1)


    def test_occurrence_types_list(self):
       # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_johnny, 'password': self.password_johnny})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        ret = c.get('/internal/occurrences/types/')

        json_ret = ret.json()

        self.assertEqual(ret.status_code, 200)
        self.assertEqual(json_ret[0]['id'], 1)
        self.assertEqual(json_ret[0]['label'], 'lbl')

    def test_occurence_creation_by_student(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_johnny, 'password': self.password_johnny})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        data = dict()
        data['anonymous'] = False
        data['title'] = 'Criando um role'
        data['content'] = 'Texto to teste de criação'
        data['type_id'] = 1

        # Create another occurrence, witch will be id=3
        ret = c.post('/internal/occurrences/create/', data)
        
        json_ret = ret.json()

        self.assertEqual(ret.status_code, 200)
        self.assertTrue(json_ret['done'])

        # Delete creation
        occur = Occurrences.objects.get(title='Criando um role')
        occur.delete()


    def test_toggle_solved(self):
        # Login
        c = APIClient()
        r = c.post('/internal/api-token-auth/',
                   {'username': self.username_emb, 'password': self.password_emb})
        r = r.json()
        c.credentials(HTTP_AUTHORIZATION='Token ' + r['token'])

        data = dict()
        data['id'] = 2
        data['action'] = 'toggle-solved'

        occur = Occurrences.objects.get(id=1)
        occur.solved = False
        occur.save()

        ret = c.post('/internal/occurrences/', data)

        json_ret = ret.json()

        self.assertEqual(ret.status_code, 200)
        self.assertTrue(json_ret['done'])
        self.assertFalse(occur.solved)