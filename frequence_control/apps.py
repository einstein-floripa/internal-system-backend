from django.apps import AppConfig


class FrequenceControlConfig(AppConfig):
    name = 'frequence_control'
