from django.db import models
from internal.models import User


class AttendanceStates(models.Model):
    description = models.CharField(max_length=30)

    def __str__(self):
        return 'ID:' + str(self.id) + ' - ' + self.description


class Attendance(models.Model):
    register_by = models.ForeignKey(User,
                                    on_delete=models.PROTECT,
                                    related_name='+')
    student = models.ForeignKey(User,
                                on_delete=models.CASCADE,
                                related_name='+')
    state = models.ForeignKey(AttendanceStates,
                              on_delete=models.PROTECT)
    action_timestamp = models.DateTimeField()
    register_timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.student.full_name + ' - ' + str(self.state) + ' - ' + \
               self.action_timestamp.strftime('%m/%d/%Y')
